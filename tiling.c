/*
  TILING_UPPER_LEFT         tiling upper_left          MAJ+ALT+v
  TILING_UPPER_CENTER_LEFT  tiling upper_center_left   MAJ+ALT+d
  TILING_UPPER_CENTER       tiling upper_center        MAJ+ALT+l
  TILING_UPPER_CENTER_RIGHT tiling upper_center_right  MAJ+ALT+j
  TILING_UPPER_RIGHT        tiling upper_right         MAJ+ALT+z

  TILING_LEFT               tiling left                MAJ+ALT+t
  TILING_CENTER_LEFT        tiling center_left         MAJ+ALT+s
  TILING_CENTER             tiling center              MAJ+ALT+r
  TILING_CENTER_RIGHT       tiling center_right        MAJ+ALT+n
  TILING_RIGHT              tiling right               MAJ+ALT+m

  TILING_LOWER_LEFT         tiling lower_left          MAJ+ALT+q
  TILING_LOWER_CENTER_LEFT  tiling lower_center_left   MAJ+ALT+g
  TILING_LOWER_CENTER       tiling lower_center_right  MAJ+ALT+h
  TILING_LOWER_CENTER_RIGHT tiling lower_center        MAJ+ALT+f
  TILING_LOWER_RIGHT        tiling lower_right         MAJ+ALT+ç

  MAJ+ALT+

  Need to:
    - switch from wayland to Xorg
    - install wmctrl
*/



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

// Used to verify if the screen is wide
int dim_cond(int resX, int resY){
    return resX > 2 * resY;
}

int main(int argc, char *argv[]){
    FILE *fp;
    char path[1035];

    int offset_X = 0; // offset because of sidebar
    int offset_Y = 0; // offset because of sidebar

    int resX;   // width of the screen
    int resY;   // height of the screen
    int X;      // X position of the windows from top left corner of the screen
    int Y;      // Y position of the windows from top left corner of the screen
    int W;      // width of the window
    int H;      // height of the window
    int BX;
    int BY;

    fp = popen("xdpyinfo | awk '/dimensions/{print$2}'", "r");
    while(fgets(path, sizeof(path), fp) != NULL){
        resX = atoi(strtok(path, "x")) - offset_X;
        resY = atoi(strtok(NULL, "x")) - offset_Y;
    }
    pclose(fp);

    fp = popen("xwininfo -id $(xdotool getactivewindow) | grep Relative | cut -c 27-", "r");
    fgets(path, sizeof(path), fp);
    BX = atoi(path);
    fgets(path, sizeof(path), fp);
    BY = atoi(path);
    pclose(fp);

    int dc = dim_cond(resX, resY);

    if(strcmp(argv[1],"upper_left") == 0){
        X = offset_X;
        if(dc){
            W = resX / 4;
        }else{
            W = resX / 2;
        }
        Y = offset_Y;
        H = resY / 2;
    }
    if(strcmp(argv[1],"upper_center") == 0){
        if(dc){
            X = offset_X + resX / 4;
            W = resX / 2;
        }else{
            X = offset_X;
            W = resX;
        }
        Y = offset_Y;
        H = resY / 2;
    }
    if(strcmp(argv[1],"upper_center_left") == 0){
        if(dc){
            X = offset_X + resX / 4;
            W = resX / 4;
        }else{
            X = offset_X;
            W = resX;
        }
        Y = offset_Y;
        H = resY / 2;
    }
    if(strcmp(argv[1],"upper_center_right") == 0){
        if(dc){
            X = offset_X + resX / 2;
            W = resX / 4;
        }else{
            X = offset_X;
            W = resX;
        }
        Y = offset_Y;
        H = resY / 2;
    }
    if(strcmp(argv[1],"lower_center_left") == 0){
        if(dc){
            X = offset_X + resX / 4;
            W = resX / 4;
        }else{
            X = offset_X;
            W = resX;
        }
        Y = offset_Y + resY / 2;
        H = resY / 2;
    }
    if(strcmp(argv[1],"lower_center_right") == 0){
        if(dc){
            X = offset_X + resX / 2;
            W = resX / 4;
        }else{
            X = offset_X;
            W = resX;
        }
        Y = offset_Y + resY / 2;
        H = resY / 2;
    }
    if(strcmp(argv[1],"upper_right") == 0){
        if(dc){
            X = offset_X + 3 * resX / 4;
            W = resX / 4;
        }else{
            X = offset_X + resX / 2;
            W = resX / 2;
        }
        Y = offset_Y;
        H = resY / 2;
    }
    if(strcmp(argv[1],"lower_left") == 0){
        X = offset_X;
        if(dc){
            W = resX / 4;
        }else{
            W = resX / 2;
        }
        Y = offset_Y + resY / 2;
        H = resY / 2;
    }
    if(strcmp(argv[1],"lower_center") == 0){
        if(dc){
            X = offset_X + resX / 4;
            W = resX / 2;
        }else{
            X = offset_X;
            W = resX;
        }
        Y = resY / 2;
        H = resY / 2;
    }
    if(strcmp(argv[1],"lower_right") == 0){
        if(dc){
            X = offset_X + 3 * resX / 4;
            W = resX / 4;
        }else{
            X = offset_X + resX / 2;
            W = resX / 2;
        }
        Y = offset_Y + resY / 2;
        H = resY / 2;
    }
    if(strcmp(argv[1],"left") == 0){
        X = offset_X;
        if(dc){
            W = resX / 4;
        }else{
            W = resX / 2;
        }
        Y = offset_Y;
        H = resY;
    }
    if(strcmp(argv[1],"center_left") == 0){
        if(dc){
            X = offset_X + resX / 4;
            W = resX / 4;
        }else{
            X = offset_X;
            W = resX / 2;
        }
        Y = offset_Y;
        H = resY;
    }
    if(strcmp(argv[1],"center_right") == 0){
        if(dc){
            X = offset_X + 2 * resX / 4;
            W = resX / 4;
        }else{
            X = offset_X;
            W = resX / 2;
        }
        Y = offset_Y;
        H = resY;
    }
    if(strcmp(argv[1],"center") == 0){
        if(dc){
            X = offset_X + resX / 4;
            W = resX / 2;
        }else{
            X = offset_X;
            W = resX;
        }
        Y = offset_Y;
        H = resY;
    }
    if(strcmp(argv[1],"right") == 0){
        if(dc){
            X = offset_X + 3 * resX / 4;
            W = resX / 4;
        }else{
            X = offset_X + resX / 2;
            W = resX / 2;
        }
        Y = offset_Y;
        H = resY;
    }
    // W = W - BX;

    H = H - BY;

    printf("Values are: %d,%d,%d,%d,%d,%d\n", X, Y, W, H, BX, BY);

    char args[1024];
    sprintf(args, "0,%d,%d,%d,%d", X, Y, W, H);

    pid_t pid = fork();
    if(pid == 0){
        execl("/usr/bin/wmctrl", "/usr/bin/wmctrl", "-r", ":ACTIVE:", "-b", "remove,maximized_vert", NULL);
    }
    else{
        int status;
        if (waitpid(pid, &status, 0) == -1){
        }
        else{
            execl("/usr/bin/wmctrl", "/usr/bin/wmctrl", "-r", ":ACTIVE:", "-e", args, NULL);
        }
    }

    return 0;
}
